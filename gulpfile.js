var gulp        = require('gulp'),
	sass        = require('gulp-sass'),
	imagemin    = require('gulp-imagemin'),
	changed     = require('gulp-changed'),
	browserSync = require('browser-sync'),
    livereload  = require('gulp-livereload'),    
    gp_concat   = require('gulp-concat'),
    gp_rename   = require('gulp-rename'),
    watch       = require('gulp-watch'),
    uglify      = require('gulp-uglify'),
    gutil       = require('gutil'),
    ngmin       = require('gulp-ngmin');
    

gulp.task('sass', function () {
    return gulp.src('./app/template/css/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
    gutil.log('SASS OK');
});

gulp.task('compress', function() {
  gulp.src([
            'bower_components/angular/angular.min.js',
            'bower_components/angular-animate/angular-animate.min.js',
            'bower_components/angular-aria/angular-aria.min.js',
            'bower_components/angular-material/angular-material.min.js',
            'bower_components/angular-messages/angular-messages.min.js',
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            './app/module.js',
            './app/controller.js',
            './app/service.js'
            ])
        .pipe(uglify({mangle: false}))
        .pipe(gp_concat('concat.js'))        
        .pipe(gp_rename('script.min.js'))
        .pipe(ngmin({dynamic: true}).on('error', function(e){
            console.log(e);
         }))
        .pipe(gulp.dest('./dist/js'));
});

gulp.task('jpg', function() {
	gulp.src('./template/img/**/*.*')
		.pipe(changed('./dist/img/'))
		.pipe(imagemin({
			progressive: true
		}))
		.pipe(gulp.dest('./dist/img/'));
});

gulp.task('browser-sync', function() {
    browserSync.init(['./dist/css/**', 'index.html'], {
        server: {
            baseDir: './',
            index: 'index.html',
            port: 3002
        },
        ui: {
            port: 3002
        }
    });
});

gulp.task('watch', ['sass', 'browser-sync'], function () { 
    gulp.watch(['./app/template/css/**/*.scss'], ['sass'],function (event) {  
        gulp.start('sass');      
    });
});