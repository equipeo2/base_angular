(function () {
    'use strict';

    angular
    .module('noticias')
    .controller('noticiasController', noticiasController)
    .filter('trustAs', function ($sce) {
        return function (input, type) {
            if (typeof input === "string") {
                return $sce.trustAs(type || 'html', input);
            }
            return "";
        };
    });

    noticiasController.$inject = ['$sce', '$filter','$timeout','$location'];

    function noticiasController($sce, $filter, $timeout,$location) {
        console.info('noticiasController Iniciado');
        var vm = this;
        
    }
})();