(function () {
    'use strict';

    angular
    .module('noticias')
    .config(Config);

    function Config($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('/noticias', {
            url: "/noticias",
            templateUrl: "moduloNoticias/template/index.html",
            controller: 'noticiasController',
            controllerAs: 'vm'
        })
        .state('/noticias/:Slugnoticias', {
            url: "/noticias/:Slugnoticias",
            templateUrl: "moduloNoticias/template/interna.html",
            controller: 'noticiasController',
            controllerAs: 'vm'
        });         
    }
})();