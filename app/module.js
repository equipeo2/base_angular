(function(){
    'use strict';
    
    angular
    .module('app', ['ui.router','ngMaterial','ngMessages','ui.mask','ui.utils.masks','noticias'])
    .config(Config)
    .config(["$locationProvider", function($locationProvider) {
        $locationProvider.html5Mode(true);
    }])
    .run(function ($rootScope, $location, $state, $stateParams) {

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            $("html, body").animate({
                scrollTop: 10
            }, 600);   
        })
    })
    .directive("repeatEnd", function(){
        return {
            restrict: "A",
            link: function (scope, element, attrs) {
                if (scope.$last) {
                    scope.$eval(attrs.repeatEnd);
                }
            }
        };
    })
    .directive('imageonload', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                    $('#loader').fadeOut('slow');
                });
                element.bind('error', function(){
                    alert('CSS error. reload');
                });
            }
        };
    })
    .factory('Conecta', ['$timeout','$q','AppService', function(time, q, Service) {
        return function(acao,api,parametro){   
            var deferred = q.defer(); 
            switch(acao) {
                case 'POST':
                    Service.apiPOST(api,parametro).then(function(response) {      
                        if(response.status == 200){        
                            deferred.resolve(response);
                        }else{
                            deferred.resolve(response.status);
                        }
                    }, function (error) {
                        console.error(error);
                    });
                break;
                case 'GET':
                    Service.apiGET(api,parametro).then(function(response) {      
                        if(response.status == 200){        
                            deferred.resolve(response.data);
                        }else{
                            deferred.resolve(response.status);
                        }
                    }, function (error) {
                        console.error(error);
                    });
                break;
                case 'PUT':
                    Service.apiPUT(api,parametro).then(function(response) {      
                        if(response.status == 200){        
                            deferred.resolve(response);
                        }else{
                            deferred.resolve(response.status);
                        }
                    }, function (error) {
                        console.error(error);
                    });
                break;
                case 'DELETE':
                    Service.apiDELETE(api,parametro).then(function(response) {      
                        if(response.status == 200){        
                            deferred.resolve(response);
                        }else{
                            deferred.resolve(response.status);
                        }
                    }, function (error) {
                        console.error(error);
                    });
                break;
                default:
                break;
            }
            return deferred.promise;                
        };
    }]);
     
    function Config($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

        $stateProvider.state('/', {
            url: "/",
            templateUrl: "app/template/app.html",
            controller: 'AppController',
            controllerAs: 'vm'
        });        
    }
})();