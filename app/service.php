<?php
include '../app/config_api.php';

switch ($_SERVER['REQUEST_METHOD']) {
     case 'POST':
        switch ($_GET['t']) {
            case 'nome do case':
                $data = json_decode(file_get_contents("php://input"));
                $data->authenticator = $token;
                $data = json_encode($data);
                $url = $url_api.'categoria/';
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data );
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                //print curl_exec($ch);
                //curl_close($ch);
                $resposta = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($status == 200){
                    print_r($resposta);
                    curl_close($ch);
                } else {
                    http_response_code($status);
                    curl_close($ch);
                }
            break;
            default:
            break;
        }
    break;

    case 'GET':
        $rows = array();
        switch ($_GET['t']) {           
            case 'listaCategorias':
                $url = $url_api.'categoria/?authenticator='.$token;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                //print curl_exec($ch);
                //curl_close($ch);
                $resposta = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($status == 200){
                    print_r($resposta);
                    curl_close($ch);
                } else {
                    http_response_code($status);
                    curl_close($ch);
                }
            break;
            default:
            break;
        }
    break;

    case 'PUT':
        $rows = array();
        switch ($_GET['t']) {           
            case 'listaCategorias':
                $dados = json_decode($_POST['dados']);
                $url = $url_api.'imagens/upload/'.$dados->id_imagem.'/?authenticator='.$token;
                $total_imagens = count($_FILES['file']['name']);
                $target_path = $_FILES['file']['tmp_name'];
                $filename = $_FILES['file']['name'];
                $filetype = $_FILES['file']['type'];
                $filedata = $_FILES['file']['tmp_name'];
                $postfields = array("filedata" => "@$filedata;type=".$filetype, "filename" => $filename);
               // $postfields = array('files' => $target_path);
                $filesize = $_FILES['file']['size'];
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_INFILESIZE, $filesize);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields );
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:multipart/form-data'));
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                print curl_exec($ch);
                curl_close($ch);
            break;
            default:
            break;
        }
    break;

    case 'DELETE':
        $rows = array();
        switch ($_GET['t']) {           
            case 'listaCategorias':
                $data = addslashes(filter_input(INPUT_GET, 'id'));
                print_r($data);
                $url = $url_api.'categoria/'.$data.'/?authenticator='.$token;
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                //print curl_exec($ch);
                //curl_close($ch);
                $resposta = curl_exec($ch);
                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if($status == 200){
                    print_r($resposta);
                    curl_close($ch);
                } else {
                    http_response_code($status);
                    curl_close($ch);
                }
            break;
            default:
            break;
        }
    break;
    default:
    break;
}
?>