(function () {
	'use strict';

	angular
	.module('app')
	.controller('AppController', AppController)
	.controller('MainController', MainController);	

	MainController.$inject = ['$timeout','$location'];
	function MainController($timeout, $location) {
		var vm = this;
		console.info('MainController Iniciado');
		vm.baseUrl = "url base do projeto";
// -------------------------------------------------------------------------------------------------------------------
		// Salva url atual em uma variável
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){ 
           	vm.urlAtual = $location.url();
        })
// -------------------------------------------------------------------------------------------------------------------
		// esconde gif de loading
        vm.loading = function () {
            $("#loader").delay(2000).fadeOut("slow");
        };
        vm.loading();
// -------------------------------------------------------------------------------------------------------------------
		// Pega ano atual para colocar no rodapé o2
		var mydate = new Date();
		vm.ano = 1900 + mydate.getYear();

	}

	AppController.$inject = ['API','$anchorScroll','$location','$timeout'];
	function AppController(API,$location,$timeout) {
		var vm = this;
		console.info('AppController Iniciado');
	    
	}
})();