(function() {
    'use strict';
    
    angular
    .module('app')
    .service('AppService', AppService);
    
    AppService.$inject = ['$http', 'API'];
    
    function AppService($http, API){
        this.apiGET = function(acao, parametro) {
            return $http.get(API.url + 'app/service.php?t='+acao+'&p='+parametro);
        };
        this.apiPOST = function(acao, parametro) {
            return $http.post(API.url + 'app/service.php?t='+acao, parametro);
        };
        this.apiPUT = function(acao, parametro) {
            return $http.put(API.url + 'app/service.php?t='+acao, parametro);
        };
        this.apiDELETE = function(acao, parametro) {
            return $http.delete(API.url + 'app/service.php?t='+acao+'&p='+parametro);
        };
    }
})();